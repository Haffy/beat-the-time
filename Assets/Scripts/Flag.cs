using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flag : MonoBehaviour
{
    private const string V = "Flag Acquired";
    public Text FlagAcquiredText;
    private static bool FlagAcquired = false;

    private void Start()
    {
        FlagAcquiredText.text = "";
    }
    void OnTriggerEnter(Collider flag)
    {
        if (flag.gameObject.tag == "Player")
        {
            print("Item picked up");
            Destroy(gameObject);

            FlagAcquired = true;
            FlagAcquiredText.text = V;
        }
        
    }
    public static bool IsFlagAcquired()
    {
        return FlagAcquired;
    }
}
